<?php

include_once __DIR__ . '/vendor/autoload.php';
define('TOKEN_FILE', 'token_info.json');

use AmoCRM\Client\AmoCRMApiClient;

session_start();

$provider = parse_ini_file('settings.ini');

$apiClient = new AmoCRMApiClient(
    $provider['clientId'],
    $provider['clientSecret'],
    $provider['redirectUri']);

if(!file_exists('settings.ini')
    || array_key_exists($provider['clientId'])
    || array_key_exists($provider['clientSecret'])
    || array_key_exists($provider['redirectUri']))
{
echo 'Пожалуста проверьте правильность настроек';
die();
}

if (isset($_GET['referer'])) {
    $apiClient->setAccountBaseDomain($_GET['referer']);
}


if (!isset($_GET['code'])) {
    $state = bin2hex(random_bytes(16));
    $_SESSION['oauth2state'] = $state;
    $authorizationUrl = $apiClient->getOAuthClient()->getAuthorizeUrl([
        'state' => $state,
        'mode'  => 'post_message',
    ]);
    header('Location: ' . $authorizationUrl);
    die;
} elseif (empty($_GET['state']) || empty($_SESSION['oauth2state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {
    unset($_SESSION['oauth2state']);
    exit('Invalid state :' . empty($_GET['state']));
}

/**
 * Ловим обратный код
 */
try {
    $accessToken = $apiClient->getOAuthClient()->getAccessTokenByCode($_GET['code']);
    if (!$accessToken->hasExpired()) {

        saveToken([
            'accessToken'  => $accessToken->getToken(),
            'refreshToken' => $accessToken->getRefreshToken(),
            'expires'      => $accessToken->getExpires(),
            'baseDomain'   => $apiClient->getAccountBaseDomain(),
        ]);
    }
} catch (Exception $e) {
    die((string)$e);
}
//$leadsService = $apiClient->leads();
$ownerDetails = $apiClient->getOAuthClient()->getResourceOwner($accessToken);
$apiClient->setAccessToken($accessToken);
printf('Привет, %s!', $ownerDetails->getName()." токен успешно получен :)<b>");
printf('<a href="/">Home</a><br>');
printf('<a href="/script.php">Запустить скрипт</a>');
die;


function saveToken($accessToken)
{

    if (
        isset($accessToken)
        && isset($accessToken['accessToken'])
        && isset($accessToken['refreshToken'])
        && isset($accessToken['expires'])
        && isset($accessToken['baseDomain'])
    ) {
        $data = [
            'accessToken' => $accessToken['accessToken'],
            'expires' => $accessToken['expires'],
            'refreshToken' => $accessToken['refreshToken'],
            'baseDomain' => $accessToken['baseDomain'],
        ];
        file_put_contents('token_info.json', json_encode($data));
    } else {
        exit('Invalid access token ' . var_export($accessToken, true));
    }
}


