<?php

include_once __DIR__ . '/vendor/autoload.php';
define('TOKEN_FILE', 'token_info.json');

use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\Models\LeadModel;
use AmoCRM\Models\TaskModel;

session_start();

$provider = parse_ini_file('settings.ini');
$apiClient = new AmoCRMApiClient(
    $provider['clientId'],
    $provider['clientSecret'],
    $provider['redirectUri']);
$accessToken = getToken();

$apiClient->setAccessToken($accessToken);
$apiClient->setAccountBaseDomain($accessToken->getValues()['baseDomain']);
$leads = $apiClient->leads()->get();
$tasks = $apiClient->tasks()->get();
echo ('Соединение успешно установленно...<br>');

try {
    /** @var LeadModel $lead */
    foreach ($leads as $lead) {
        if (!$lead->getClosestTaskAt()) {
            $task = creatTask($lead->getId(),$apiClient);
            echo ('Сделке '.$lead->getId().' | '.$lead->getName().' назначена новая задача # '
                .$task->getId()." | ".$task->getText().'<br>');
        }
    }
} catch (Exception $e) {
    dump($e->getMessage());
}

/**
 * @param int $id
 * @param AmoCRMApiClient $apiClient
 * @return TaskModel
 */
function creatTask($id, $apiClient)
{

    /** @var TaskModel $task */
    $task    = new TaskModel();
    $nextDay = time() + (24 * 60 * 60);
    $task->setCompleteTill($nextDay);
    $task->setEntityType('leads');
    $task->setEntityId($id);
    $task->setText('Сделка без задачи');
    $apiClient->tasks()->addOne($task);
    return $task;
}


/**
 * @return \League\OAuth2\Client\Token\AccessToken
 */
function getToken()
{
    $accessToken = json_decode(file_get_contents(TOKEN_FILE), true);
    if (
        isset($accessToken)
        && isset($accessToken['accessToken'])
        && isset($accessToken['refreshToken'])
        && isset($accessToken['expires'])
        && isset($accessToken['baseDomain'])
    ) {
        echo ('Запрос токена из tonek_info.json<br>');
        return new \League\OAuth2\Client\Token\AccessToken([
            'access_token'  => $accessToken['accessToken'],
            'refresh_token' => $accessToken['refreshToken'],
            'expires'       => $accessToken['expires'],
            'baseDomain'    => $accessToken['baseDomain'],
        ]);
    } else {
        exit('Invalid access token ' . var_export($accessToken, true));
    }
}

die;

